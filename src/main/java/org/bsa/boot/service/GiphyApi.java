package org.bsa.boot.service;

import org.bsa.boot.config.ApiKey;
import org.bsa.boot.entity.Gif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.json.*;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Paths;

@Component
public class GiphyApi {

    private final ApiKey apiKey;
    private final HttpClient httpClient;

    @Value("${api.translate}")
    private String rawTranslateLink;

    @Value("${api.download-begin}")
    private String downloadLinkBegin;
    @Value("${api.download-end}")
    private String downloadLinkEnd;

    @Autowired
    public GiphyApi(ApiKey apiKey, HttpClient httpClient) {
        this.apiKey = apiKey;
        this.httpClient = httpClient;
    }

    private String getQueryLink(String query) {
        return rawTranslateLink + query;
    }

    private HttpRequest buildGetRequest(String url) {
        return HttpRequest
                .newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
    }
    private HttpRequest buildTranslateRequest(String query) {
        return buildGetRequest(getQueryLink(query));
    }
    private HttpRequest buildDownloadRequest(String id) {
        return buildGetRequest(downloadLinkBegin + id + downloadLinkEnd);
    }

    public Gif getQueryGif(String query) {
        try {
            var stringResponse = httpClient.send(buildTranslateRequest(query), HttpResponse.BodyHandlers.ofString()).body();
            var jsonResponse = new JSONObject(stringResponse);
            var id = jsonResponse.getJSONObject("data").getString("id");
            var fileResponse = httpClient.send(buildDownloadRequest(id), HttpResponse.BodyHandlers.ofFile(Paths.get("temp.gif"))).body();
            return new Gif(query, id, fileResponse.toFile());
        } catch (IOException | InterruptedException | JSONException ex) {
            return null;
        }
    }

}
