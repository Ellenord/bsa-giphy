package org.bsa.boot.service;


import org.bsa.boot.dto.*;
import org.bsa.boot.entity.User;
import org.bsa.boot.repository.CacheRepository;
import org.bsa.boot.repository.UsersRepository;
import org.bsa.boot.util.GifDtoMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OperationService {

    private final CacheRepository cacheRepository;
    private final UsersRepository usersRepository;
    private final GifDtoMapper gifDtoMapper;
    private final GiphyApi giphyApi;

    public OperationService(CacheRepository cacheRepository, UsersRepository usersRepository, GifDtoMapper gifDtoMapper, GiphyApi giphyApi) {
        this.cacheRepository = cacheRepository;
        this.usersRepository = usersRepository;
        this.gifDtoMapper = gifDtoMapper;
        this.giphyApi = giphyApi;
    }

    public List < GifQueryFilesDto > getCache(String query) {
        cacheRepository.loadCache();
        var cache = cacheRepository.getAll();
        if (query == null) {
            return gifDtoMapper.mapCacheToDto(cache);
        }
        if (!cache.containsKey(query)) {
            cache.clear();
            return gifDtoMapper.mapCacheToDto(cache);
        }
        var queryGifs = cache.get(query);
        cache.clear();
        cache.put(query, queryGifs);
        return gifDtoMapper.mapCacheToDto(cache);
    }

    public GifQueryFilesDto cacheGenerate(String query) {
        var gif = giphyApi.getQueryGif(query);
        cacheRepository.addGif(gif);
        gif.getFile().delete();
        return gifDtoMapper.mapQueryToDto(query, cacheRepository.getQueryGifs(query));
    }

    public void clearCache() {
        cacheRepository.clearCache();
    }

    public GifFilesDto getGifs() {
        cacheRepository.loadCache();
        var cache = cacheRepository.getAll();
        var idList = new TreeSet< String >();
        cache.forEach((key, value) -> value.forEach(gif ->
                idList.add("/cache/" + gif.getName() + "/" + gif.getId() + ".gif")));
        return new GifFilesDto(idList.stream().collect(Collectors.toList()));
    }

    public List < GifQueryFilesDto > getUserGifs(String name) {
        usersRepository.loadUser(name);
        var users = usersRepository.getAll();
        if (!users.containsKey(name)) {
            return new ArrayList<>();
        }
        List < GifQueryFilesDto > userGifs = new ArrayList<>();
        users.get(name).getGifs().forEach(
                (key, value) -> userGifs.add(
                        new GifQueryFilesDto(key, value.stream().map(
                                gif -> "/cache/" + gif.getName() + "/" + gif.getId() + ".gif")
                                    .collect(Collectors.toList()))));
        return userGifs;
    }

    public PathDto userGenerate(String id, GenerateQueryDto generateQueryDto) {
        if (usersRepository.getUserByName(id) == null) {
            usersRepository.addUser(new User(id, new File("./users/" + id + "/history.csv"), new HashMap<>()));
        }
        if (!generateQueryDto.isForce()) {
            var gif = cacheRepository.getGifByName(generateQueryDto.getQuery());
            if (gif != null) {
                usersRepository.addGifToUser(id, gif);
                cacheRepository.addGif(gif);
                return new PathDto("/cache/" + gif.getName() + "/" + gif.getId() + ".gif");
            }
        }
        var gif = giphyApi.getQueryGif(generateQueryDto.getQuery());
        usersRepository.addGifToUser(id, gif);
        cacheRepository.addGif(gif);
        gif.getFile().delete();
        return new PathDto("/cache/" + gif.getName() + "/" + gif.getId() + ".gif");
    }

    public List < UserHistoryDto > getUserHistory(String userName) {
        var history = usersRepository.getUserHistory(userName);
        List < UserHistoryDto > historyDtos = new ArrayList<>();
        try {
            var scanner = new Scanner(history);
            while (scanner.hasNextLine()) {
                var lineData = scanner.nextLine().split(",");
                historyDtos.add(new UserHistoryDto(lineData[0], lineData[1], lineData[2]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return historyDtos;
    }

    public void clearUserHistory(String userName) {
        var history = usersRepository.getUserHistory(userName);
        history.delete();
        try {
            history.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(String userName) {
        usersRepository.deleteUser(userName);
    }

    public void localQueryUserGifDelete(String userName, String query) {
        usersRepository.localQueryDelete(userName, query);
    }

    public PathDto findGif(String userName, String query, boolean force) {
        if (force) {
            usersRepository.loadUser(userName);
        }
        var user = usersRepository.getUserByName(userName);
        if (user == null || !user.getGifs().containsKey(query)) {
            return new PathDto("not found");
        }
        return new PathDto("/cache/" + query + "/" + user.getGifs().get(query).get(0).getId() + ".gif");
    }

}
