package org.bsa.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.net.http.HttpClient;

@Configuration
public class ApiConfiguration {

    @Bean
    public ApiKey apiKey(ApiKeyConfigurationProperties configurationProperties) {
        return new ApiKey(configurationProperties.getKey());
    }

    @Bean
    @Scope(value = "prototype")
    public HttpClient httpClient() {
        return HttpClient.newHttpClient();
    }

}
