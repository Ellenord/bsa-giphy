package org.bsa.boot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "api")
@Setter
@Getter
public class ApiKeyConfigurationProperties {
    private String key;
}
