package org.bsa.boot.config;

import lombok.Getter;

@Getter
public class ApiKey {
    private final String key;
    public ApiKey(String key) {
        this.key = key;
    }
}
