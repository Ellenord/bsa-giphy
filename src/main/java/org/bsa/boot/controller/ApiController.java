package org.bsa.boot.controller;

import org.bsa.boot.dto.*;
import org.bsa.boot.service.OperationService;
import org.bsa.boot.util.GifDtoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class ApiController {

    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    private final OperationService operationService;
    private final GifDtoMapper gifDtoMapper;

    @Autowired
    public ApiController(OperationService operationService, GifDtoMapper gifDtoMapper) {
        this.operationService = operationService;
        this.gifDtoMapper = gifDtoMapper;
    }

    @GetMapping("/cache")
    public List< GifQueryFilesDto > cacheGetRequestsHandler(@RequestHeader("X-BSA-GIPHY") String header, @RequestParam Map < String, String > query) {
        logger.info("cacheGetRequestsHandler");
        if (query.isEmpty() || !query.containsKey("query")) {
            return operationService.getCache(null);
        } else {
            return operationService.getCache(query.get("query"));
        }
    }

    @PostMapping("/cache/generate")
    public GifQueryFilesDto cacheGeneratePostRequestHandler(@RequestHeader("X-BSA-GIPHY") String header,
                                                            @RequestBody QueryDto queryDto)
    {
        logger.info("cacheGeneratePostRequestHandler");
        return operationService.cacheGenerate(queryDto.getQuery());
    }

    @DeleteMapping("/cache")
    public void cacheDeleteRequestHandler(@RequestHeader("X-BSA-GIPHY") String header) {
        logger.info("cacheDeleteRequestHandler");
        operationService.clearCache();
    }

    @GetMapping("/gifs")
    public GifFilesDto gifsGetRequestHandler(@RequestHeader("X-BSA-GIPHY") String header) {
        logger.info("gifsGetRequestHandler");
        return operationService.getGifs();
    }

    @GetMapping("/user/{id}/all")
    public List < GifQueryFilesDto > userGifsGetRequestHandler(@RequestHeader("X-BSA-GIPHY") String header, @PathVariable String id) {
        logger.info("userGifsGetRequestHandler");
        return operationService.getUserGifs(id);
    }

    @PostMapping("/user/{id}/generate")
    public PathDto gifGeneratePostRequestHandler(
            @RequestHeader("X-BSA-GIPHY") String header,
            @PathVariable String id, @RequestBody GenerateQueryDto generateQueryDto)
    {
        logger.info("gifGeneratePostRequestHandler");
        return operationService.userGenerate(id, generateQueryDto);
    }

    @GetMapping("/user/{id}/history")
    public List < UserHistoryDto > userHistoryGetRequestHandler(@RequestHeader("X-BSA-GIPHY") String header,
                                                                @PathVariable String id) {
        logger.info("userHistoryGetRequestHandler");
        return operationService.getUserHistory(id);
    }

    @DeleteMapping("/user/{id}/history/clean")
    public void userHistoryCleanDeleteRequestHandler(@RequestHeader("X-BSA-GIPHY") String header,
                                                     @PathVariable String id) {
        logger.info("userHistoryCleanDeleteRequestHandler");
        operationService.clearUserHistory(id);
    }

    @DeleteMapping("/user/{id}/clean")
    public void userCleanDeleteRequestHandler(@RequestHeader("X-BSA-GIPHY") String header,
                                              @PathVariable String id) {
        logger.info("userCleanDeleteRequestHandler");
        operationService.deleteUser(id);
    }

    @DeleteMapping("/user/{id}/reset")
    public void userResetDeleteRequestHandler(@RequestHeader("X-BSA-GIPHY") String header,
                                              @PathVariable String id, @RequestParam Map < String, String > query) {
        logger.info("userResetDeleteRequestHandler");
        if (query.isEmpty() || !query.containsKey("query")) {
            operationService.localQueryUserGifDelete(id,null);
        } else {
            operationService.localQueryUserGifDelete(id, query.get("query"));
        }
    }

    @GetMapping("/user/{id}/search")
    public PathDto userHistoryCleanDeleteRequestHandler(
            @RequestHeader("X-BSA-GIPHY") String header,
            @PathVariable String id, @RequestParam Map < String, String > query) {

        logger.info("userHistoryCleanDeleteRequestHandler");
        if (!query.containsKey("query") || !query.containsKey("force")) {
            return new PathDto("error");
        } else {
            return operationService.findGif(id, query.get("query"), query.get("force").equals("true"));
        }
    }

}
