package org.bsa.boot.util;

import org.bsa.boot.entity.Gif;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class GifManager {

    public static Gif gifCopy(Gif gif, String path) {
        try {
            Files.copy(gif.getFile().toPath(), new File(path).toPath());
        } catch (IOException e) {}
        return new Gif(gif.getName(), gif.getId(), new File(path));
    }

}
