package org.bsa.boot.util;

import org.bsa.boot.dto.GifQueryFilesDto;
import org.bsa.boot.entity.Gif;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class GifDtoMapper {

    public List < GifQueryFilesDto > mapCacheToDto(Map < String, List < Gif > > cache) {
        return cache.keySet().stream().map(key -> mapQueryToDto(key, cache.get(key))).collect(Collectors.toList());
    }

    public GifQueryFilesDto mapQueryToDto(String query, List < Gif > gifs) {
        return new GifQueryFilesDto(query,
                gifs.stream().map(gif -> "/cache/" + gif.getName() + "/" + gif.getId() + ".gif")
                        .collect(Collectors.toList()));
    }

}
