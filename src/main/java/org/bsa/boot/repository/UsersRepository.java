package org.bsa.boot.repository;

import org.bsa.boot.entity.Gif;
import org.bsa.boot.entity.User;
import org.bsa.boot.util.GifManager;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UsersRepository {

    private static final Map < String, User > users = new HashMap<>();

    public Map < String, User > getAll() {
        return new HashMap<>(users);
    }

    public void loadUser(String userName) {
        if (!new File("./users/" + userName).exists()) {
            return;
        }
        users.put(userName, new User(userName, new File("./users/" + userName + "/history.csv"), new HashMap<>()));
        for (var i : Arrays.asList(new File("./users/" + userName).listFiles())) {
            if (!i.isDirectory()) {
                continue;
            }
            users.get(userName).getGifs().put(i.getName(), new ArrayList<>());
            for (var j : Arrays.asList(new File("./users/" + userName + "/" + i.getName()).listFiles())) {
                users.get(userName).getGifs().get(i.getName()).add(new Gif(i.getName(), j.getName().replace(".gif", ""), j));
            }
        }
    }

    public void addUser(User user) {
        if (users.containsKey(user.getName())) {
            return;
        }
        if (new File("./users/" + user.getName()).exists()) {
            loadUser(user.getName());
            return;
        }
        new File("./users/" + user.getName()).mkdir();
        try {
            user.getLog().createNewFile();
            user.getLog().renameTo(new File("./users/" + user.getName() + "/history.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Map.Entry < String, User > i : users.entrySet()) {
            for (Map.Entry<String, List<Gif>> j : i.getValue().getGifs().entrySet()) {
                new File("./users/" + user.getName() + "/" + j.getKey()).mkdir();
            }
        }
        users.put(user.getName(), user);
    }

    public User getUserByName(String name) {
        if (!users.containsKey(name)) {
            return null;
        } else {
            return users.get(name);
        }
    }

    public void addGifToUser(String userName, Gif gif) {
        if (!users.containsKey(userName) ||
                (users.get(userName).getGifs().containsKey(gif.getName())
                        && users.get(userName).getGifs().get(gif.getName())
                            .stream().map(i -> i.getId()).collect(Collectors.toList())
                                .contains(gif.getId())))
        {
            return;
        }
        new File("./users/" + userName + "/" + gif.getName()).mkdir();
        gif = GifManager.gifCopy(gif, "./users/" + userName + "/" + gif.getName() + "/" + gif.getId() + ".gif");
        if (!users.get(userName).getGifs().containsKey(gif.getName())) {
            users.get(userName).getGifs().put(gif.getName(), new ArrayList<>());
        }
        users.get(userName).getGifs().get(gif.getName()).add(gif);
        addToHistory(userName, java.time.LocalDate.now().toString() + "," + gif.getName()
                + ",/cache/" + gif.getName() + "/" + gif.getId() + ".gif\n");
    }

    public File getUserHistory(String userName) {
        if (new File("./users/" + userName).exists()) {
            loadUser(userName);
        }
        if (users.containsKey(userName)) {
            return users.get(userName).getLog();
        } else {
            return null;
        }
    }

    public void addToHistory(String userName, String event) {
        if (!users.containsKey(userName)) {
            return;
        }
        if (!users.get(userName).getLog().exists()) {
            try {
                users.get(userName).getLog().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Files.write(Paths.get("./users/" + userName + "/history.csv"), event.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void deleteUser(String userName) {
        if (new File("./users/" + userName).exists()) {
            loadUser(userName);
        }
        if (!users.containsKey(userName)) {
            return;
        }
        for (Map.Entry < String, List < Gif > > i : users.get(userName).getGifs().entrySet()) {
            for (var j : i.getValue()) {
                j.getFile().delete();
            }
            new File("./users/" + userName + "/" + i.getKey()).delete();
        }
        new File("./users/" + userName + "/history.csv").delete();
        new File("./users/" + userName).delete();
        users.remove(userName);
    }

    public void localQueryDelete(String username, String query) {
        if (!users.containsKey(username)) {
            return;
        }
        if (query == null) {
            users.remove(username);
            return;
        }
        users.get(username).getGifs().remove(query);
    }

}
