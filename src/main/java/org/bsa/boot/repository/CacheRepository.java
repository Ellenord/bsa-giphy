package org.bsa.boot.repository;

import org.bsa.boot.entity.Gif;
import org.bsa.boot.util.GifManager;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.*;

@Repository
public class CacheRepository {

    private static final Map < String, List < Gif > > cache = new HashMap<>();

    public CacheRepository() { }

    public void clearCache() {
        for (Map.Entry < String, List <Gif> > i : cache.entrySet()) {
            for (var j : i.getValue()) {
                j.getFile().delete();
            }
            new File("./cache/" + i.getKey() + "/").delete();
        }
        cache.clear();
    }

    public List < Gif > getQueryGifs(String query) {
        if (!cache.containsKey(query)) {
            return new ArrayList<>();
        }
        return cache.get(query);
    }

    public void addGif(Gif gif) {
        if (!cache.containsKey(gif.getName())) {
            cache.put(gif.getName(), new ArrayList<>());
            new File("./cache/" + gif.getName() + "/").mkdir();
        }
        gif = GifManager.gifCopy(gif, "./cache/" + gif.getName() + "/" + gif.getId() + ".gif");
        cache.get(gif.getName()).add(gif);
    }

    public Map < String, List < Gif > > getAll() {
        return new HashMap<>(cache);
    }

    public Gif getGifByName(String name) {
        if (!cache.containsKey(name) || cache.get(name).isEmpty()) {
            return null;
        } else {
            return cache.get(name).get(0);
        }
    }

    public void loadCache() {
        List < File > cacheDirs = Arrays.asList(new File("./cache/").listFiles());
        for (var i : cacheDirs) {
            cache.put(i.getName(), new ArrayList<>());
            for (var j : Arrays.asList(i.listFiles())) {
                cache.get(i.getName()).add(new Gif(i.getName(), j.getName().replace(".gif", ""), j));
            }
        }
    }

}
