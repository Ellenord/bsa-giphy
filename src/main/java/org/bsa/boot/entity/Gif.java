package org.bsa.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.*;

@Data
@AllArgsConstructor
public class Gif {
    String name;
    String id;
    File file;
}
