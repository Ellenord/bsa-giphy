package org.bsa.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.util.*;

@Data
@AllArgsConstructor
public class User {
    private final String name;
    private final File log;
    private final Map < String, List < Gif > > gifs;
}
