package org.bsa.boot.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class GifFilesDto {
    private final List < String > gifs;
}
