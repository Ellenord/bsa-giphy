package org.bsa.boot.dto;

import lombok.Getter;
import lombok.Value;

@Value
@Getter
public class GenerateQueryDto {
    private final String query;
    private final boolean force;
}