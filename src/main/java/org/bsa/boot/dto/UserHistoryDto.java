package org.bsa.boot.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class UserHistoryDto {
    private final String date;
    private final String query;
    private final String gif;
}
