package org.bsa.boot.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class UserDto {
    private final String name;
    private final List< GifQueryFilesDto > gifs;
}
